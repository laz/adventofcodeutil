package adventofcodeutil

import (
	"bufio"
	"io"
	"os"
)

// GetScannerFromArgs Return a *bufio.Scanner tied to a file
// indicated as os.Args[1] or if no argument provided use
// os.Stdin. Also returns a cleanup func() to call when the
// scanner is finished being read from.
func GetScannerFromArgs() (*bufio.Scanner, func(), error) {
	r, f, err := GetReaderFromArgs()
	if err != nil {
		return nil, f, err
	}
	return bufio.NewScanner(r), f, nil
}

// GetReaderFromArgs Return an io.Reader tied to a file
// indicated as os.Args[1] or if no argument provided use
// os.Stdin. Also returns a cleanup func() to call when the
// scanner is finished being read from.
func GetReaderFromArgs() (io.Reader, func(), error) {
	r := os.Stdin
	c := func() {}
	if len(os.Args) > 1 {
		if f, err := os.Open(os.Args[1]); err == nil {
			r = f
			c = func() { r.Close() }
		} else {
			return nil, c, err
		}
	}
	return r, c, nil
}

// StringFunc alias for funcs that take a *bufio.Scanner and
// return a string and an error.
// DEPRECATED use StringFuncScanner instead
type StringFunc func(*bufio.Scanner) (string, error)

// IntFunc alias for funcs that take a *bufio.Scanner and
// return a int and an error.
// DEPRECATED use IntFuncScanner instead
type IntFunc func(*bufio.Scanner) (int, error)

// ExecuteStringFunc Invokes a StringFunc.
// DEPRECATED use ExecuteStringFuncScanner instead
func ExecuteStringFunc(f StringFunc) (string, error) {
	s, c, err := GetScannerFromArgs()
	if err != nil {
		return "", err
	}
	defer c()
	return f(s)
}

// ExecuteIntFunc Invokes an IntFunc.
// DEPRECATED use ExecuteIntFuncScanner instead
func ExecuteIntFunc(f IntFunc) (int, error) {
	s, c, err := GetScannerFromArgs()
	if err != nil {
		return 0, err
	}
	defer c()
	return f(s)
}

// StringFuncScanner alias for funcs that take a *bufio.Scanner and
// return a string and an error.
type StringFuncScanner func(*bufio.Scanner) (string, error)

// IntFuncScanner alias for funcs that take a *bufio.Scanner and
// return a int and an error.
type IntFuncScanner func(*bufio.Scanner) (int, error)

// ExecuteStringFuncScanner Invokes a StringFuncScanner.
func ExecuteStringFuncScanner(f StringFuncScanner) (string, error) {
	s, c, err := GetScannerFromArgs()
	if err != nil {
		return "", err
	}
	defer c()
	return f(s)
}

// ExecuteIntFuncScanner Invokes an IntFuncScanner.
func ExecuteIntFuncScanner(f IntFuncScanner) (int, error) {
	s, c, err := GetScannerFromArgs()
	if err != nil {
		return 0, err
	}
	defer c()
	return f(s)
}

// StringFuncReader alias for funcs that take a io.Reader and
// return a string and an error.
type StringFuncReader func(io.Reader) (string, error)

// IntFuncReader alias for funcs that take a io.Reader and
// return a int and an error.
type IntFuncReader func(io.Reader) (int, error)

// ExecuteStringFuncReader Invokes a StringFuncReader.
func ExecuteStringFuncReader(f StringFuncReader) (string, error) {
	s, c, err := GetReaderFromArgs()
	if err != nil {
		return "", err
	}
	defer c()
	return f(s)
}

// ExecuteIntFuncReader Invokes an IntFuncReader.
func ExecuteIntFuncReader(f IntFuncReader) (int, error) {
	s, c, err := GetReaderFromArgs()
	if err != nil {
		return 0, err
	}
	defer c()
	return f(s)
}
